---
title: "Miguel"
date: 2021-09-18T23:27:57-03:00
draft: false
image: ../../images/migu.png
---
Posição: Fixo

Idade: 21

Miguel, Mr. Presidente, 'Mah', ou para os mais íntimos, MIGAS!! Além de falar fluentemente francês e ter rebixado para a Prod, o nosso fixo não facilita em nada a vida dos adversários. Não é PM mas se vc bobear ele te joga nas grades.